interface Question {
    question: string;
    choices: string[];
    correctAnswer: number;
  }
   
  const questions: Question[] = [
    {
      question: 'What is the output of the following code? \n\n console.log(typeof null);',
      choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to add one or more elements to the end of an array?',
      choices: ['push()', 'join()', 'slice()', 'concat()'],
      correctAnswer: 0,
    },
    {
      question: 'What is the result of the following expression?\n\n3 + 2 + "7"',
      choices: ['"327"', '"12"', '"57"', '"NaN"'],
      correctAnswer: 2,
    },
    {
      question: 'What is the purpose of the "use strict" directive in JavaScript?',
      choices: ['Enforce stricter type checking', 'Enable the use of modern syntax', 'Enable strict mode for improved error handling', 'Disable certain features for better performance'],
      correctAnswer: 2,
    },
    {
      question: 'What is the scope of a variable declared with the "let" keyword?',
      choices: ['Function scope', 'Global scope', 'Block scope', 'Module scope'],
      correctAnswer: 2,
    },
    {
      question: 'Which higher-order function is used to transform elements of an array into a single value?',
      choices: ['map()', 'filter()', 'reduce()', 'forEach()'],
      correctAnswer: 2,
    },
    {
      question: 'What does the "=== " operator in JavaScript check for?',
      choices: ['Equality of values', 'Equality of values and types', 'Inequality of values', 'Reference equality'],
      correctAnswer: 1,
    },
    {
      question: 'What is the purpose of the "this" keyword in JavaScript?',
      choices: ['Refer to the current function', 'Refer to the parent function', 'Refer to the global object', 'Refer to the object that owns the current code'],
      correctAnswer: 3,
    },
    {
      question: 'What does the "NaN" value represent in JavaScript?',
      choices: ['Not a Number', 'Null', 'Negative Number', 'Not Applicable'],
      correctAnswer: 0,
    },
    {
      question: 'Which method is used to remove the last element from an array?',
      choices: ['pop()', 'shift()', 'slice()', 'splice()'],
      correctAnswer: 0,
    },
    
  ];
  let count = 0
  let nameQus = ""
  const ex2 = document.getElementById('ex2') as HTMLDivElement
  const p1 = document.createElement('p')
  p1.innerText = "Current Score: "+ count +"/10"

  ex2.appendChild(p1)
  for (const qus of questions) {
    const result = []
    let sum = ""
    let isTrue = false
    let i = 1
    const div = document.createElement('div')
    const span = document.createElement('span')
    const text = document.createTextNode(qus.question)
    span.appendChild(text)
    div.appendChild(span)
    ex2.appendChild(div)
    for (const choic of qus.choices) {
      const div2 = document.createElement('div')
      const span2 = document.createElement('span')
      const choice = document.createTextNode(choic)
      const radios = document.createElement('input')
      radios.type = "radio"
      radios.name = qus.question
      
      radios.addEventListener("click",()=>{
        sum = choic
        nameQus = radios.name
        // console.log(result)
      })
      
      div2.appendChild(radios)
      span2.appendChild(choice)
      div2.appendChild(span2)
      ex2.appendChild(div2)
    }
    const div3 = document.createElement('div')
      const btnsub = document.createElement('button')
      const textsub = document.createTextNode('submit')
      btnsub.addEventListener("click", ()=>{
        if (sum == ""){
          alert("please choose an answer first!")

        }else {
          i = 0
          for(const indexChoi of qus.choices){
            if (indexChoi == sum){
              isTrue = true
              if (i == qus.correctAnswer){
                alert("Correct!")
                count++
                p1.textContent = "Current Score: "+ count +"/10"
                isTrue = false
                console.log(count)

              }else {
                alert("Incorrect!")
                isTrue = false

              }
              btnsub.disabled = true
            }
            i++
            // if (isTrue == true){
            //   for (const qus2 of questions){
            //     //console.log(nameQus)
            //     if (qus2.question == nameQus){
            //       console.log(nameQus)
            //       if (i == qus2.correctAnswer){
            //         alert("Correct!")
            //         isTrue = false
            //         return
            //       }else {
            //         alert("Incorrect!")
            //         isTrue = false
            //         return
            //       }
            //     }
            //   }
            // }
          }
                
            }
          }
          // for (const qus2 of questions){
          //   console.log(nameQus)
          //   if (qus2.question == nameQus){
          //     // console.log(nameQus)
          //     if (i == qus2.correctAnswer){
          //       alert("Correct!")
          //     }else {
          //       alert("Incorrect!")
          //     }
          //   }
          // }
          )

      btnsub.appendChild(textsub)
      div3.appendChild(btnsub)
      ex2.appendChild(div3)
}
export {}
